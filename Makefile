CFLAGS=-Wall -Wextra -pedantic -Werror -O0 -g
LDFLAGS=-static
OUTPUT=invoke
SOURCES=$(wildcard ./*.c)
$(OUTPUT):p_spawn.c invoke.c run.h p_spawn.h
	$(CC) $(CFLAGS) $(SOURCES) -o $@ $(LDFLAGS)
clean:
	rm -f $(OUTPUT)
