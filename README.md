# Dumb invoke

A suckless-like commands runner

# Usage

Place your commands in run.h (see repo's run.h). Make binary and start it.

To edit command list see run.h.

## What needs to be added
- ~~Base directory resolve~~
- Who can run?
- ~~Rollback/Undo support~~
- ~~Parallel exec support~~
- Basic syscalls like cd,chown etc
- Default actions like save text to file, rename, replace
## Anything else?
*NB: can run via TCC (tcc -run ./target.c)*
