#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "run.h"
#ifdef USE_PARALLEL
#include "p_spawn.h"
#endif
#ifdef NO_USE_SHELL
#include <sys/wait.h>
#endif

static const char *index_filename = "resume_index.txt";

static int chdir_base(char *exepath);
static void cleanup();
static void dump_sequences();
static int invoke_handler(const struct script_cmd *command);
static int launch_sequence(const struct sequence_decriptor *seqd);
#ifdef USE_PARALLEL
static int parallel_handler(const struct spawn_list *sl);
#endif
#ifdef USE_PUT_FILE
static int put_file_handler(const struct put_file *file);
#endif
static int register_failure(unsigned int index);
static unsigned int resume_index();
static const struct sequence_decriptor *select_sequence(const char *sel_key);

int
chdir_base(char *exename)
{
  char *slashp = strrchr(exename, '/');
  if (slashp == NULL) return -1;
  *slashp = '\0';
  int res = chdir(exename);
  *slashp = '/';
  return res;
}

void
cleanup()
{
  struct stat s;
  int r = stat(index_filename, &s);
  if (r == -1) {
    if (errno == ENOENT) return;
    fprintf(stderr, "[!] ATTENTION: inspect file %s in working directory\n"
        " stat() failed: %s\n", index_filename, strerror(errno));
    return;
  }
  if (unlink(index_filename) == -1) {
    fprintf(stderr, "[!] ATTENTION: cannot unlink %s "
        "in working directory during cleanup\n"
        " unlink() failed: %s\n", index_filename, strerror(errno));
  }
}

static void
dump_sequences()
{
  puts("Available sequences:");
  for (size_t t = 0;
       t < (sizeof(sequence_selector) / sizeof(sequence_selector[0])); ++ t) {
    printf(" %s\n", sequence_selector[t].sequence_name);
    for (size_t v = 0; v < sequence_selector[t].count; ++ v) {
#ifdef USE_PARALLEL
      if (sequence_selector[t].commands[v].title == spawn) {
        const struct spawn_list *sp =
            (const struct spawn_list *)
            sequence_selector[t].commands[v].commandline;
        if (sp != NULL && sp->n > 0) {
          printf("  => Spawn:\n    %s", *sp->commands);
          for (size_t c = 1; c < sp->n; ++ c)
            printf(",\n    %s", sp->commands[c]);
          printf("\n");
        }
        continue;
      }
#endif
#ifdef USE_PUT_FILE
      if (sequence_selector[t].commands[v].title == put_file) {
        printf("  => Write out file \"%s\"\n",
               FILES_TO_PUT[ (unsigned long)
               (sequence_selector[t].commands[v].commandline) ].name);
        continue;
      }
#endif
      printf("=> %s : %s\n", sequence_selector[t].commands[v].title,
             sequence_selector[t].commands[v].commandline);
    }
  }
}

int
invoke_handler(const struct script_cmd *command)
{
  fprintf(stderr, "%s%s%s\n", OUTLINE_HEAD, command->title, OUTLINE_TAIL);
#ifdef NO_USE_SHELL
#define ARGCOUNT 240
  char *arguments[ARGCOUNT];
  char *cmd_dup = strdup(command->commandline);
  char *head = cmd_dup;
  char *spc = strchr(cmd_dup, ' ');
  arguments[0] = cmd_dup;
  size_t c_arg = 1;
  while (spc) {
    *spc = '\0';
    arguments[c_arg] = head;
    head = spc + 1;
    spc = strchr(head, ' ');
    ++ c_arg;
  }
  arguments[c_arg] = head;
  arguments[c_arg + 1] = NULL;
  pid_t pid = fork();
  if (pid == -1) {
    free(cmd_dup);
    return -1;
  }
  if (pid == 0) {
    execvp(arguments[0], &arguments[1]);
    free(cmd_dup);
    return -1;
  }
  free(cmd_dup);
  int status;
  waitpid(pid, &status, 0);
  if (WIFEXITED(status)) {
    return WEXITSTATUS(status);
  } else {
    return -1;
  }
#else
  return system(command->commandline);
#endif
}

int
launch_sequence(const struct sequence_decriptor *seqd)
{
  size_t first = seqd->ignores_restart ? 0 : resume_index();
  const struct script_cmd *pcmd = seqd->commands;
  size_t seqind = first;
  for (; seqind < seqd->count; ++ seqind) {
#ifdef USE_PUT_FILE
    if (seqd->commands[seqind].title == put_file) {
      if (put_file_handler(&FILES_TO_PUT[(unsigned long)
                           (pcmd[seqind].commandline)])
          != 0) {
        if (!seqd->ignores_errors) {
          goto _err_exit;
        }
      }
      continue;
    }
#endif
#ifdef USE_PARALLEL
    if (pcmd[seqind].title == spawn) {
      if (parallel_handler((const struct spawn_list *)pcmd[seqind].commandline)
          != 0)
        if (!seqd->ignores_errors) {
          goto _err_exit;
        }
      continue;
    }
#endif
    if (pcmd[seqind].title == ch_dir) {
      if (chdir(pcmd[seqind].commandline) == -1) {
        fprintf(stderr, "%s Unable to change directory to %s ", ERROR_OUTLINE,
                pcmd[seqind].commandline);
        perror("chdir call failed");
        if (!seqd->ignores_errors) goto _err_exit;
      } else {
        char *dynbuffer = NULL;
        char statbuffer[240];
        int ok = 0;
        if (getcwd(statbuffer, sizeof(statbuffer)) == NULL) {
          if (errno == ERANGE) {
            dynbuffer = malloc(PATH_MAX);
            if (getcwd(dynbuffer, PATH_MAX) != NULL) ok = 1;
          }
        } else {
          ok = 1;
        }
        if (ok)
          fprintf(stderr, "%s %s\n", NEW_DIR_OUTLINE,
                  dynbuffer ? dynbuffer : statbuffer);
        else
          perror("Warning: can't get current directory path");
        if (dynbuffer) free(dynbuffer);
      }
      continue;
    }
    if (invoke_handler(&pcmd[seqind]) != 0) {
      fprintf(stderr, "%s %s\n", ERROR_OUTLINE, pcmd[seqind].commandline);
      if (!seqd->ignores_errors) {
        goto _err_exit;
      }
    }
  }
  return 0;
 _err_exit:
  register_failure(seqind);
  return -1;
}

int
main(int argc, char **argv)
{
  if (argc > 1) {
    if (strcmp(argv[1], "-dump") == 0) {
      dump_sequences();
      return 0;
    }
  } else {
    fprintf(stderr, "Usage: %s <-dump> | -<sequence_name>\n", argv[0]);
    return 1;
  }
  if (chdir_base(argv[0]) != 0) {
    perror("chdir failed");
    return 1;
  }
  const struct sequence_decriptor *target_sequence =
      select_sequence(argv[1] + 1);
  if (target_sequence == NULL) {
    fprintf(stderr, "Failed to select sequence, aborting");
    return 1;
  }
  int result = 0;
  if (launch_sequence(target_sequence) != 0) {
    result = 1;
  } else {
    cleanup();
  }
  printf(result == 0 ? "Done\n" : "Run complete, there were errors\n");
  return result;
}

#ifdef USE_PARALLEL
int parallel_handler(const struct spawn_list *sl)
{
  if (sl->n > MAX_PARALLEL_PROCESSES) {
    fprintf(stderr, "%s Trying to spawn too much parallel processes (%u), "
            "aborting", ERROR_OUTLINE, sl->n);
    return -1;
  }
  int out_pids[MAX_PARALLEL_PROCESSES] = { 0 };
  int out_indexes[MAX_PARALLEL_PROCESSES];
  for (size_t i = 0; i < MAX_PARALLEL_PROCESSES; ++ i) out_indexes[i] = -1;
  return process_parallel(sl->commands, out_pids, out_indexes, sl->n);
}
#endif

#ifdef USE_PUT_FILE
int
put_file_handler(const struct put_file *file)
{
  fprintf(stderr, "%s Putting file %s%s\n", OUTLINE_HEAD, file->name,
          OUTLINE_TAIL);
  FILE *f = fopen(file->name, "w");
  if (!f) return -1;
  fwrite(file->text, 1, strlen(file->text), f);
  int result = ferror(f);
  fclose(f);
  return result;
}
#endif

int
register_failure(unsigned int index)
{
  FILE *file = fopen(index_filename, "w");
  if (file == NULL) {
    return -1;
  }
  int res = fprintf(file, "%i", index);
  fclose(file);
  if (res < 1) {
    if (unlink(index_filename) == -1) {
      fprintf(stderr, "[!] ATTENTION:"
          " logic failure, cannot unlink %s\nPlease run rm"
          " %s before next tool run to avoid problems!\n", index_filename,
          index_filename);
    }
  }
  return res;
}

unsigned int
resume_index()
{
  FILE *file = fopen(index_filename, "r");
  if (file == NULL) return 0;
  char buf[42];
  int n = fread(buf, 1, sizeof(buf) - 1, file);
  fclose(file);
  if (n < 1) return 0;
  buf[n] = '\0';
  return abs(atoi(buf));
}

const struct sequence_decriptor *
select_sequence(const char *sel_key)
{
  for (size_t i = 0; i < sizeof(sequence_selector) / sizeof(*sequence_selector);
       ++ i) {
    if (strcmp(sel_key, sequence_selector[i].sequence_name) == 0)
      return &sequence_selector[i];
  }
  return NULL;
}
