#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#include "p_spawn.h"

static const char *failure_filename = "failed_indexes.txt";

#define DO_N(varname, times) \
  for (counter varname = 0; varname < times; ++ varname)

static int spawn(const char *cmd, pid_t *out_pid);
static int spawn_process_list(const char **cmds, pid_t *pids, counter count,
                              const int *restart_indexes);
static int store_failed_indexes(int *indexes, counter max_count);

static int
scan_next_index(const char **input_start, const char *input_end)
{
  const char *scan = *input_start;
  if (!isdigit(*scan)) return -1;
  while (*scan != '\n') {
    if (scan == input_end && *scan != '\n') return -1;
    ++ scan;
  }
  /* Allow indexes in range 0..99. */
  char num_index[3] = {'\0'};
  if (scan - *input_start > 2) return -1;
  num_index[0] = **input_start;
  if (scan - *input_start > 1) num_index[1] = (*input_start)[1];
  *input_start = scan + 1;
  return atoi(num_index);
}

static int
load_failed_indexes(int *indexes, counter max_count)
{
  struct stat s;
  if (stat(failure_filename, &s) != -1 && S_ISREG(s.st_mode)) {
    FILE *f = fopen(failure_filename, "r");
    if (f == NULL) return -1;
    /* Sane maximum file size: 10 (for 0..9) +
     * 90 * 2 (for 10 - 99, 2 chars per number) +
     * 99 (newline at end of each number). */
    char buffer[10 + 90 * 2 + 99];
    unsigned long n = fread(buffer, 1, sizeof(buffer), f);
    if (n) {
      const char *last = buffer + n;
      const char *head = buffer;
      int idx;
      for (counter i = 0; i < max_count && head < last; ++ i) {
        idx = scan_next_index(&head, last);
        if (idx < 0) {
          fclose(f);
          return -1;
        }
        indexes[i] = idx;
      }
    }
    fclose(f);
  }
  return 0;
}

int
process_parallel(const char **commands, int *pids, int *indexes, counter count)
{
  int *restart = NULL;
  if (load_failed_indexes(indexes, count) < 0) {
    fprintf(stderr, "%s %s\n", REPORT_ERROR_OUTLINE,
            "Bad input in indexes file");
    return -1;
  }
  if (indexes[0] != -1) restart = indexes;
  spawn_process_list(commands, pids, count, restart);
  int status;
  int exit_code;
  size_t cf_idx = 0;
  int success = 0;
  const char *command_report_str = NULL;
  DO_N(i, count) {
    waitpid(pids[i], &status, 0);
    if (WIFEXITED(status)) {
      exit_code = WEXITSTATUS(status);
      if (exit_code != 0 && success == 0) success = 1;
      if (restart) {
        command_report_str = commands[*restart];
        if (exit_code == 0) *restart = -1;
        ++ restart;
        if (*restart == -1) {
          fprintf(stderr, "%s %s\n",
                  !exit_code ? REPORT_OK_OUTLINE : REPORT_ERROR_OUTLINE,
                  command_report_str);
          break;
        }
      } else {
        command_report_str = commands[i];
        if (exit_code != 0) indexes[cf_idx] = i;
        ++ cf_idx;
      }
      fprintf(stderr, "%s %s\n",
              exit_code ? REPORT_ERROR_OUTLINE : REPORT_OK_OUTLINE,
              command_report_str);
    }
  }
  store_failed_indexes(indexes, count);
  return success;
}

int
spawn(const char *cmd, pid_t *out_pid)
{
  *out_pid = fork();
  if (*out_pid == -1) return -1;

  if (*out_pid == 0) {
    execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);
    return errno;
  } else {
    fprintf(stderr, "%s%s%s\n", SPAWN_OUTLINE_HEAD, SPAWN_OUTLINE_TAIL, cmd);
  }
  return 0;
}

int
spawn_process_list(const char **cmds, pid_t *pids, counter count,
                   const int *restart_indexes)
{
  if (restart_indexes) {
    for (counter c = 0; c < count && restart_indexes[c] != -1; ++ c) {
      if (spawn(cmds[restart_indexes[c]], &pids[c]) != 0) return -1;
    }
    return 0;
  }
  DO_N(i, count) {
    if (spawn(cmds[i], &pids[i]) != 0) return -1;
  }
  return 0;
}

int
store_failed_indexes(int *indexes, counter max_count)
{
  FILE *f = NULL;
  unsigned int failed_count = 0;
  DO_N(n, max_count) {
    if (indexes[n] != -1) {
      if (f == NULL) {
        f = fopen(failure_filename, "w");
        if (f == NULL) return -1;
      }
      fprintf(f, "%i\n", indexes[n]);
      ++ failed_count;
    }
  }
  if (f) fclose(f);
  struct stat s;
  if (failed_count == 0 && stat(failure_filename, &s) != -1
      && S_ISREG(s.st_mode)) {
    unlink(failure_filename);
  }
  return 0;
}
