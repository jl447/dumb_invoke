typedef unsigned int counter;

#define SPAWN_OUTLINE_HEAD "\033[1;33m spawn>"
#define SPAWN_OUTLINE_TAIL "\033[0m"
#define REPORT_OK_OUTLINE "[\033[32mOK\033[0m]"
#define REPORT_ERROR_OUTLINE "[\033[31mERROR\033[0m]"

int
process_parallel(const char **commands, int *pids, int *indexes, counter count);
