#define USE_PARALLEL
#define USE_PUT_FILE
#define NO_USE_SHELL

#define OUTLINE_HEAD "\033[1;32m====="
#define OUTLINE_TAIL "=====\033[0m"
#define ERROR_OUTLINE "[\033[31mERROR\033[0m]"
#define NEW_DIR_OUTLINE "[\033[1;32mNEWDIR\033[0m]"

#ifdef USE_PUT_FILE
static const char put_file[] = "fput";
#endif
#ifdef USE_PARALLEL
static const char spawn[] = "sp";

struct spawn_list {
  const char **commands;
  unsigned int n;
};

#define MAX_PARALLEL_PROCESSES 16
#endif

struct script_cmd {
  const char *title;
  const char *commandline;
};

static const char ch_dir[] = "cd";

#ifdef USE_PUT_FILE

struct put_file {
  const char *name;
  const char *text;
};

static const char cool_license_txt[] =
"COOL LICENSE v0.0\n"
"\n"
"You may do whatever you want with these source code files, but:\n"
" - You MAY NOT RESTRICT OTHERS to do anything with these source code files.\n";

static struct put_file my_files[] = {
  {
    "LICENSE",
    cool_license_txt
  }
};

#define FILES_TO_PUT my_files
#endif

static const char *commands_git[] = {
  "git clone https://gitlab.com/jl447/anycolour.git",
  "git clone https://gitlab.com/jl447/desktop-linux-kernel-config.git",
  "git clone https://gitlab.com/jl447/file_to_c_string.git",
  "git clone https://gitlab.com/jl447/weblink_catalog.git",
  "git clone https://gitlab.com/jl447/dircompare.git"
};

static const struct spawn_list git_list = {
  commands_git,
  sizeof(commands_git) / sizeof(const char *)
};

static struct script_cmd run_sequence[] = {
  /* { "Breaking build", "ls /root" }, */
  { spawn, (const char *)&git_list },
  { ch_dir, "anycolour" },
  { "Compiling anycolour", "make -j4" },
  { ch_dir, "../weblink_catalog" },
  { "Compiling Weblink Catalog", "make -j4" },
  { ch_dir, "../file_to_c_string" },
  { "Compiling File To C String", "make -j4" },
  { ch_dir, "../dircompare" },
  { "Compiling Dircompare", "make -j4" },
  { ch_dir, ".." },
  { put_file, (const char *)0 }
};

static struct script_cmd undo_sequence[] = {
  { "Removing anycolour directory", "rm -rf anycolour" },
  { "Removing kernel config directory", "rm -rf desktop-linux-kernel-config" },
  { "Removing Weblink Catalog directory", "rm -rf weblink_catalog" },
  { "Removing File To C String directory", "rm -rf file_to_c_string" },
  { "Removing Dircompare directory", "rm -rf dircompare" },
  { "Removing file LICENSE", "rm -f LICENSE" }
};

#define SCRIPTS run_sequence
#define INVOKE_COUNT sizeof(run_sequence) / sizeof(struct script_cmd)

#define SCRIPTS_UNDO undo_sequence
#define UNDO_COUNT sizeof(undo_sequence) / sizeof(struct script_cmd)
static const struct sequence_decriptor {
  const char *sequence_name;
  const struct script_cmd *commands;
  unsigned int count;
  int ignores_restart;
  int ignores_errors;
} sequence_selector[] = {
  {
    "default", run_sequence, INVOKE_COUNT, 0, 0
  },
  {
    "undo", undo_sequence, UNDO_COUNT, 1, 0
  }
};
